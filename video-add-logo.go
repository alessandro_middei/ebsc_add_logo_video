package main

import (
	"fmt"
	"image"
	"image/png"
	"io/ioutil"
	"os"
	"os/exec"
	"path/filepath"
	"strconv"
	"strings"

	"gopkg.in/yaml.v2"

	//"bytes"

	"context"

	"github.com/gookit/color"
	"github.com/nfnt/resize"
)

// Area di definizione delle costanti e dei messaggi di errore

var ctx = context.Background()

const help = `

esempio di uso: video-add-logo $video [position] [logo]

[position] se non definito sarà tr -> Top Right
  il primo valore è il posizionamento sull'asse y (Top / Center / Bottom)
  il secondo valore è il posizionamento sull'asse x (Laft / Center / Right)
  			Matrice Valori
			tl | tc | tr
			cl | cc | cr
			bl | bc | br

			[logo] se non definito è di default quello nazionale dei vvf
  altrimenti va indicato il full path e deve essere in formato png, verrà poi fatto il resize a paddingxpadding
`

//Config è il file di configurazione
type Config struct {
	OutputDir string `yaml:"output_video"`
	Logo      string `yaml:"logo"`
}

func probeVideo(nome string) (int, int) {
	//ffprobe -v error -select_streams v:0 -show_entries stream=width,height -of csv=s=x:p=0 input.mp4
	cmd, err := exec.Command("ffprobe", "-v", "error", "-select_streams", "v:0", "-show_entries", "stream=width,height", "-of", "csv=s=x:p=0", nome).Output()
	if err != nil {
		color.Red.Println("Errore recuparendo height: %v", err)
		return 0, 0
	}
	strDim := string(cmd[:len(cmd)-1])
	dimensions := strings.Split(strDim, "x")
	tempXDim, err := strconv.Atoi(dimensions[0])
	if err != nil {
		color.Red.Println("Errore recuparendo height: %v", err)
	}
	tempYDim, err := strconv.Atoi(dimensions[1])
	if err != nil {
		color.Red.Println("Errore recuparendo height: %v", err)
	}
	return tempXDim, tempYDim
}

func calculatelogoPosition(xDim, yDim, lDim int, posizione string) (int, int) {
	fullLogo := lDim
	halfLogo := lDim / 2
	padding := 30
	switch posizione {
	case "tl":
		//Calcoliamo il posizionamento togliendo le del bordo di padding px
		lxDim := padding
		lyDim := padding
		return lxDim, lyDim
	case "cl":
		//Calcoliamo il posizionamento dividendo l'immagine in 2
		// -> Se il risultato è pari facciamo che x è uguale diviso 2 - 150
		// -> Se il risultato è dispari facciamo che x è uguale diviso 2 -150 +1
		// per y facciamo y-padding per il bordo
		lyDim := yDim/2 - halfLogo
		lxDim := padding
		return lxDim, lyDim
	case "bl":
		//Calcoliamo il posizionamento togliendo dal bordo i padding px
		lxDim := padding
		lyDim := yDim - fullLogo - padding
		return lxDim, lyDim
	case "tc":
		lxDim := (xDim / 2) - halfLogo
		lyDim := padding
		return lxDim, lyDim
	case "cc":
		lxDim := (xDim / 2) - halfLogo
		lyDim := (yDim / 2) - halfLogo
		return lxDim, lyDim
	case "bc":
		lxDim := (xDim / 2) - halfLogo
		lyDim := yDim - (fullLogo + padding)
		return lxDim, lyDim
	case "tr":
		//Calcoliamo il posizionamento togliendo le del bordo di padding px
		//lxDim := int(float64(xDim)*pxtopoint) + padding
		lxDim := xDim - (fullLogo + padding)
		lyDim := padding
		return lxDim, lyDim
	case "cr":
		lyDim := (yDim / 2) - halfLogo
		lxDim := xDim - (fullLogo + padding)
		return lxDim, lyDim
	case "br":
		//Calcoliamo il posizionamento togliendo le dimensioni del logo e un bordo di padding px
		lxDim := xDim - (fullLogo + padding)
		lyDim := yDim - (fullLogo + padding)
		return lxDim, lyDim
	default:
		//Calcoliamo il posizionamento togliendo le dimensioni del logo e un bordo di padding px
		lxDim := int(float64(xDim)) - (fullLogo + padding)
		lyDim := int(float64(yDim)) - (fullLogo + padding)
		return lxDim, lyDim
	}
}

func calculateMaxLogoDimension(x, y int) int {
	if x > y {
		max := (y / 3) - (y / 100 * 5)
		return max
	} else if x < y {
		max := (x / 3) - (x / 100 * 5)
		return max
	} else {
		max := (y / 3) - (y / 100 * 5)
		return max
	}
}

func verifyLogoDimension(x, y, max int) bool {
	if x >= max && y >= max {
		return false
	} else {
		return true
	}
}

func calculateDimension(immagine string) (int, int) {
	reader, err := os.Open(immagine)
	if err != nil {
		color.Red.Println("Errore nell'aprire la foto: %v", err)
	}

	img, _, err := image.DecodeConfig(reader)
	if err != nil {
		return 0, 0 // handle error somehow
	}
	//color.Blue.Println("DEBUG: Dimensioni orginali: %v %v", img.Width, img.Height)
	return img.Width, img.Height
}

func resizeImage(immagine string, max int) {
	srcFile, err := os.Open(immagine)
	if err != nil {
		color.Red.Println("Errore nell'aprire: %v", err)
	}
	src, err := png.Decode(srcFile)
	if err != nil {
		color.Red.Println("Errore nel render di: %v", err)
	}

	m := resize.Resize(uint(max), uint(max), src, resize.Lanczos3)

	out, err := os.Create("./tmp/resized_logo.png")
	if err != nil {
		color.Red.Println("Errore nel lavorare: %v", err)
	}
	defer out.Close()

	// write new image to file
	png.Encode(out, m)
	//logoXDim, logoYDim := calculateDimension("./tmp/resized_logo.png")
	//color.Blue.Println("DEBUG: misure logo resized: %v %v", logoXDim, logoYDim)
}

func applyWatermark(video, logo, dir string, xPosition, yPosition int) {
	//ffmpeg -i test.mp4 -i watermark.png -filter_complex "overlay=10:10" test1.mp4
	//color.Blue.Println("DEBUG: Posizione del logo: %v %v", xPosition, yPosition)
	overlay := "overlay=" + strconv.Itoa(xPosition) + ":" + strconv.Itoa(yPosition)
	//fmt.Println(overlay)
	outVideo := fmt.Sprintf("%s/%s_%s", dir, "logo", filepath.Base(video))
	cmd := exec.Command("ffmpeg", "-y", "-i", video, "-i", logo, "-preset", "veryfast", "-filter_complex", overlay, outVideo)
	err := cmd.Run()
	if err != nil {
		color.Red.Println("Errore nell'applicare il watermark: ", err)

	}
	color.Green.Println("Logo Applicato con successo")
}

func main() {
	// Check del file di configurazione
	filename, _ := filepath.Abs("config/config.yml")
	yamlFile, err := ioutil.ReadFile(filename)
	var config Config
	err = yaml.Unmarshal(yamlFile, &config)
	if err != nil {
		panic(err)
	}
	// Funzione di lettura e ordinamento dell'input
	args := os.Args[1:]

	// Se non ci sono parametri ritorna errore
	if len(args) < 1 {
		color.Green.Println(help)
		return
	}

	// Se il primo argomento è help ritorna messaggio di help
	if args[0] == "help" {
		color.Green.Println(help)
		return
	}
	length := len(args)
	switch length {
	case 1:
		//color.Blue.Println("DEBUG: Un argomento applica logo default")
		tempXDim, tempYDim := probeVideo(args[0])                   // Dimensioni del video
		maxDim := calculateMaxLogoDimension(tempXDim, tempYDim)     // Dimensione massima logo
		logoXDim, logoYDim := calculateDimension(config.Logo)       // Dimensione logo
		checkDim := verifyLogoDimension(logoXDim, logoYDim, maxDim) //  Verifica che il logo sia minore di 1/3 della dimensione più piccola
		//color.Blue.Println("DEBUG: verifico valore checkDim: ", checkDim)
		if checkDim { // Se logo ok
			lxDim, lyDim := calculatelogoPosition(tempXDim, tempYDim, logoXDim, "br") // Calcola dove posizionare il logo
			applyWatermark(args[0], config.Logo, config.OutputDir, lxDim, lyDim)      //Apllica logo
		} else { // Se logo non ok
			//color.Blue.Println("DEBUG: faccio il resize del logo")
			resizeImage(config.Logo, maxDim)                                                  // Resize logo
			lxDim, lyDim := calculatelogoPosition(tempXDim, tempYDim, maxDim, "br")           // Calcola posizione
			applyWatermark(args[0], "./tmp/resized_logo.png", config.OutputDir, lxDim, lyDim) // Applica logo
		}

	case 2:
		//color.Blue.Println("DEBUG: Due argomenti filmato e posizione, applica logo default")
		tempXDim, tempYDim := probeVideo(args[0])                   // Dimensioni del video
		maxDim := calculateMaxLogoDimension(tempXDim, tempYDim)     // Dimensione massima logo
		logoXDim, logoYDim := calculateDimension(config.Logo)       // Dimensione logo
		checkDim := verifyLogoDimension(logoXDim, logoYDim, maxDim) //  Verifica che il logo sia minore di 1/3 della dimensione più piccola
		//color.Blue.Println("DEBUG: verifico valore checkDim: ", checkDim)
		if checkDim { // Se logo ok
			lxDim, lyDim := calculatelogoPosition(tempXDim, tempYDim, logoXDim, args[1]) // Calcola dove posizionare il logo
			applyWatermark(args[0], config.Logo, config.OutputDir, lxDim, lyDim)         //Apllica logo
		} else { // Se logo non ok
			//color.Blue.Println("DEBUG: faccio il resize del logo")
			resizeImage(config.Logo, maxDim)                                                  // Resize logo
			lxDim, lyDim := calculatelogoPosition(tempXDim, tempYDim, maxDim, args[1])        // Calcola posizione
			applyWatermark(args[0], "./tmp/resized_logo.png", config.OutputDir, lxDim, lyDim) // Applica logo
		}
	case 3:
		//fmt.Println("Tre valori, logo posizione e logo nuovo")
		//color.Blue.Println("DEBUG: Due argomenti filmato e posizione, applica logo default")
		tempXDim, tempYDim := probeVideo(args[0])                   // Dimensioni del video
		maxDim := calculateMaxLogoDimension(tempXDim, tempYDim)     // Dimensione massima logo
		logoXDim, logoYDim := calculateDimension(args[2])           // Dimensione logo
		checkDim := verifyLogoDimension(logoXDim, logoYDim, maxDim) //  Verifica che il logo sia minore di 1/3 della dimensione più piccola
		//color.Blue.Println("DEBUG: verifico valore checkDim: ", checkDim)
		if checkDim { // Se logo ok
			lxDim, lyDim := calculatelogoPosition(tempXDim, tempYDim, logoXDim, args[1]) // Calcola dove posizionare il logo
			applyWatermark(args[0], args[2], config.OutputDir, lxDim, lyDim)             //Apllica logo
		} else { // Se logo non ok
			//color.Blue.Println("DEBUG: faccio il resize del logo")
			resizeImage(args[2], maxDim)                                                      // Resize logo
			lxDim, lyDim := calculatelogoPosition(tempXDim, tempYDim, maxDim, args[1])        // Calcola posizione
			applyWatermark(args[0], "./tmp/resized_logo.png", config.OutputDir, lxDim, lyDim) // Applica logo
		}
	}
}

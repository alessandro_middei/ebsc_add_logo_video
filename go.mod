module video-add-logo

go 1.16

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/gookit/color v1.4.2
	github.com/kr/pretty v0.1.0 // indirect
	github.com/nfnt/resize v0.0.0-20180221191011-83c6a9932646
	gopkg.in/check.v1 v1.0.0-20190902080502-41f04d3bba15 // indirect
	gopkg.in/yaml.v2 v2.4.0
)

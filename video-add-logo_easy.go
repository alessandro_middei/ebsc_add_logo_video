package main

import (
	"fmt"
	"io/ioutil"
	"os"
	"os/exec"
	"path/filepath"

	"gopkg.in/yaml.v2"

	//"bytes"

	"context"

	"github.com/gookit/color"
)

// Area di definizione delle costanti e dei messaggi di errore

var ctx = context.Background()

const help = `
 _____________________________________________________________
|  #######                    ######                          |
|  #       #    # # #         #     # #####    ##   # #    #  |
|  #       #    # # #         #     # #    #  #  #  # ##   #  |
|  #####   #    # # #         ######  #    # #    # # # #  #  |
|  #       #    # # #         #     # #####  ###### # #  # #  |
|  #        #  #  # #         #     # #   #  #    # # #   ##  |
|  #######   ##   # ######    ######  #    # #    # # #    #  |
|_____________________________________________________________|

esempio di uso: video_logo $video [position] [logo]
[position] se non definito sarà tr -> Top Right
  il primo valore è il posizionamento sull'asse y (Top / Center / Bottom)
	il secondo valore è il posizionamento sull'asse x (Laft / Center / Right)
  Matrice Valori
	tl | tc | tr
	cl | cc | cr
	bl | bc | br
[logo] se non definito è di default quello nazionale dei vvf
  altrimenti va indicato il full path e deve essere in formato png, verrà poi fatto il resize a paddingxpadding
`

//Config è il file di configurazione
type Config struct {
	OutputDir string `yaml:"output_video"`
	Logo      string `yaml:"logo"`
}

func main() {
	// Check del file di configurazione
	filename, _ := filepath.Abs("config/config.yml")
	yamlFile, err := ioutil.ReadFile(filename)
	var config Config
	err = yaml.Unmarshal(yamlFile, &config)
	if err != nil {
		panic(err)
	}
	// Funzione di lettura e ordinamento dell'input
	args := os.Args[1:]

	// Se non ci sono parametri ritorna errore
	if len(args) < 1 {
		color.Green.Println(help)
		return
	}

	// Se il primo argomento è help ritorna messaggio di help
	if args[0] == "help" {
		color.Green.Println(help)
		return
	}

	//color.Blue.Println("DEBUG: Due argomenti filmato e posizione, applica logo default")
	dir := config.Logo
	logo := config.OutputDir
	video := args[0]
	overlay := "overlay=30:30"
	//fmt.Println(overlay)
	outVideo := fmt.Sprintf("%s/%s_%s", dir, "logo", filepath.Base(video))
	cmd := exec.Command("ffmpeg", "-y", "-i", video, "-i", logo, "-preset", "veryfast", "-filter_complex", overlay, outVideo)
	err = cmd.Run()
	if err != nil {
		color.Red.Println("Errore nell'applicare il watermark: ", err)

	}
	color.Green.Println("Logo Applicato con successo")
}

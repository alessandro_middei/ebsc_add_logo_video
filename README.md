# EBSC - Plugins
## Plugin di aggiunta logo ai video

### Meta Dati


| Versione | Code Name | Data Rilascio |
| --- | --- | --- |
| 0.1 | rocky | 12-07-2021 |

### Descrizione

Implementazione in GO dell'aggiunta logo ad un video.
Da pensare se vale la pena una versione RUST per foto più grandi e plugins premium

### Esempio di Utilizzo

````bash
video-add-logo $video [position] [logo]
````
#### Position
[position] se non definito sarà tr -> Top Right

Il primo valore è il posizionamento sull'asse y (Top / Center / Bottom), il secondo valore è il posizionamento sull'asse x (Laft / Center / Right)
          
##### Matrice Valori

| tl | tc | tr |
| cl | cc | cr |
| bl | bc | br |

#### Logo
[logo] se non definito è quello presente nella cartella config altrimenti va indicato il full path e deve essere in formato png, verrà poi fatto il resize a paddingxpadding